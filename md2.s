	.text
	.align	2
	.global	matmul
	.type	matmul, %function




/*Function implements naive matrix multiplication algorithm (O(n^3))*/
matmul:
	push {r4-r11,lr}
	cmp r1, r3
	
	movne r0, #1
	bne end

	/*Iterator over left matrix rows*/
	mov r4, #0

	/*Iterator over right matrix columns*/
	mov r5, #0

	/*Iterator over left matrix columns*/
	mov r6, #0

	/*Right matrix width*/
	ldr r7, [sp, #36]

	/*Result matrix address*/
	/*ldr r10, [sp, #44]*/

	while_outer:
	cmp r4, r0
	bge while_outer_end

		while_middle:
		cmp r5, r7
		bge while_middle_end

		/*Set the result matrix cell value to 0*/
		ldr r10, [sp, #44]

		mov r8, r7
		mul r8, r8, r4

		mov r9, #4
		mul r8, r8, r9

		mul r9, r9, r5

		add r8, r8, r9
		
		mov r9, #0
		str r9, [r10, r8]


			while:
			cmp r6, r1
			bge while_end

			/*Calculate result matrix cell value step by step*/
			mov r6, r6, lsl #2
			mov r5, r5, lsl #2
			mov r1, r1, lsl #2

			mul r11, r1, r4
			add r11, r11, r6

			/*Cell value from the left matrix*/
			ldr r11, [r2, r11]

			mul r9, r7, r6
			add r9, r9, r5

			ldr r10, [sp, #40]

			/*Cell value from the right matrix*/
			ldr r9, [r10, r9]

			/*Multiply cell from the left with cell from the right*/
			mul r9, r9, r11

			/*Load current result matrix cell value*/
			ldr r10, [sp, #44]
			ldr r11, [r10, r8]

			/*Add multiplication result to the current value and store it*/
			add r11, r11, r9
			str r11, [r10, r8]

			mov r6, r6, lsr #2
			mov r5, r5, lsr #2
			mov r1, r1, lsr #2
			add r6, r6, #1

			b while
			while_end:

		mov r6, #0
		add r5, r5, #1

		b while_middle
		while_middle_end:

	mov r5, #0
	add r4, r4, #1

	b while_outer
	while_outer_end:



	mov r0, #0
end:
	pop {r4-r11, lr}
	bx lr
